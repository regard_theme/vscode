# Regard - VSCode/VSCodium

The VSCode/VSCodium version of the regard theme. More info [here][3].  


If you have any suggestion or modifications feel free to open an issue. To add a new language specific support please do a merge request.

## Install

- You can install the extension using the vscode marketplace or open-vsx for vscodium.

- You can also install the extension following this steps
    - Type &lt;CTRL+P&gt; in vscode/vscodium
    - Write `ext install nasmevka.regard`
    - Press &lt;Enter&gt;, you're done.

## A closer look
<div align="center">
    <img src="imgs/code1.png" />
    <p>function definition example</p>
</div>

<div align="center">
    <img src="imgs/code2.png" />
    <p>class definition example</p>
</div>



## Copyright & Licenses
I used the [nord theme][1] as a template, so it just normal that there is their license in the repository.  
***
[Theme License][2]  
[Nord License](./NORD_LICENSE.md)  

[1]: https://github.com/arcticicestudio/nord-visual-studio-code
[2]: https://gitlab.com/regard_theme/readme/-/blob/main/LICENSE
[3]: https://gitlab.com/regard_theme/readme
